package main

import (
	"crypt"
	"data"
	"file"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"net/http"
	"web"
)

/* view for error */
type ErrorView struct {
	Error string /* message of error */
}

/* view for table */
type TableView struct {
	Number    int    /* number of table */
	Reference string /* reference to web-page to watch database */
	Name      string /* name of table */
}

/* view for user */
type UserView struct {
	Number int    /* number of user */
	Name   string /* name of user */
}

/* view for student */
type StudentView struct {
	Number    int    /* number of student */
	Name      string /* name of student */
	Surname   string /* surname of student */
	DateBirth string /* date of birth for student */
	Email     string /* email of student */
}

const (
	entryHTMLFile      = "entry.html"      /* entry web-page */
	mainHTMLFile       = "main.html"       /* main web-page */
	dbUsersHTMLFile    = "dbUsers.html"    /* web-page to watch users */
	dbStudentsHTMLFile = "dbStudents.html" /* web-page to watch students */
)

var (
	userVariables   *file.UserVariables   /* structure for user variables */
	handlerDatabase *data.HandlerDatabase /* structure to handle database */
)

/* function to check user's data to entry in system */
func entryPageCheck(writer http.ResponseWriter, request *http.Request) {

	var (
		err       error     /* variable for error */
		login     string    /* login of user */
		password  string    /* password of user */
		hash      int       /* hash of password */
		errorView ErrorView /* variable for view of error */
		ind       bool      /* indicator for checking of user */
	)

	/* parse form */
	err = request.ParseForm()
	file.HandleErrorFile(err)

	/* receive login and password */
	login = request.FormValue("login")
	password = request.FormValue("password")

	/* calculate hash */
	hash = crypt.CalculateHash(password)
	/* check user */
	ind = handlerDatabase.CheckUserDatabase(login, hash)

	/* show error if user does not exist */
	if ind {
		/* make redirect */
		http.Redirect(writer, request, "/main", 301)
	} else {
		/* message of error */
		errorView.Error = "Incorrect data!"
		/* load entry page (with error) */
		web.PageLoad(writer, entryHTMLFile, errorView)
	}

}

/* function to load entry page */
func entryPageInit(writer http.ResponseWriter, request *http.Request) {

	/* load entry page */
	web.PageLoad(writer, entryHTMLFile, nil)

}

/* function to load main page */
func mainPageInit(writer http.ResponseWriter, request *http.Request) {

	var (
		tablesView []TableView /* view of tables */
	)

	/* check referrer */
	if web.CheckWebReference(request, (*userVariables).IpAddress, (*userVariables).PortServer, "/") {
		return
	}

	/* create and fill view */
	tablesView = make([]TableView, 0, 100)
	tablesView = append(tablesView, TableView{1, "/users", "Users"})
	tablesView = append(tablesView, TableView{2, "/students", "Students"})

	/* load web-page */
	web.PageLoad(writer, mainHTMLFile, tablesView)

}

/* function to load page to watch users from database */
func dbUsersPageInit(writer http.ResponseWriter, request *http.Request) {

	var (
		usersView []UserView   /* view of users */
		users     *[]data.User /* users of database */
		i         int          /* iterator */
	)

	/* check referrer */
	if web.CheckWebReference(request, (*userVariables).IpAddress, userVariables.PortServer, "/main") {
		return
	}

	/* get users from database */
	users = handlerDatabase.GetUsers()

	/* create and fill view for users */
	usersView = make([]UserView, len(*users))
	i = 0
	for i < len(*users) {
		usersView[i].Number = i + 1
		usersView[i].Name = (*users)[i].Name
		i++
	}

	/* load web-page */
	web.PageLoad(writer, dbUsersHTMLFile, usersView)

}

/* function to load page to watch students from database */
func dbStudentsPageInit(writer http.ResponseWriter, request *http.Request) {

	var (
		studentsView []StudentView   /* view of students */
		students     *[]data.Student /* students of database */
		i            int             /* iterator */
	)

	/* check referrer */
	if web.CheckWebReference(request, (*userVariables).IpAddress, userVariables.PortServer, "/main") {
		return
	}

	/* get students from database */
	students = handlerDatabase.GetStudents()

	/* create and fill view for students */
	studentsView = make([]StudentView, len(*students))
	i = 0
	for i < len(*students) {
		studentsView[i].Number = i + 1
		studentsView[i].Name = (*students)[i].Name
		studentsView[i].Surname = (*students)[i].Surname
		studentsView[i].DateBirth = (*students)[i].DateBirth
		studentsView[i].Email = (*students)[i].Email
		i++
	}

	/* load web-page */
	web.PageLoad(writer, dbStudentsHTMLFile, studentsView)

}

/* entry point */
func main() {

	var (
		router *mux.Router /* router for pages */
		err    error       /* variable for error */
	)

	/* write initial message */
	fmt.Println("Server for information system")

	/* read configuration file with writing of message */
	fmt.Println("Read configuration file...")
	userVariables = new(file.UserVariables)
	file.ReadConfigFile(userVariables)

	/* create handler of database */
	handlerDatabase, err = data.NewHandlerDatabase(userVariables.NameDB, userVariables.DataDB)
	file.HandleErrorFile(err)

	/* create and fill router for pages with writing of message */
	fmt.Println("Launching of server...")
	router = mux.NewRouter()
	router.HandleFunc("/", entryPageInit).Methods("GET")
	router.HandleFunc("/", entryPageCheck).Methods("POST")
	router.HandleFunc("/main", mainPageInit)
	router.HandleFunc("/database/users", dbUsersPageInit)
	router.HandleFunc("/database/students", dbStudentsPageInit)

	/* launch http-server for working with writing of message */
	fmt.Println("Server is working...")
	http.Handle("/", router)
	err = http.ListenAndServe((*userVariables).IpAddress+":"+(*userVariables).PortServer, nil)
	file.HandleErrorFile(err)

	/* close handler of database */
	fmt.Println("Server finishes working...")
	handlerDatabase.Close()

}
