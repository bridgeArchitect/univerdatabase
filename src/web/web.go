package web

import (
	"file"
	"html/template"
	"net/http"
)

func PageLoad(writer http.ResponseWriter, filenameHTML string, objectView interface{}) {

	var (
		tmpl *template.Template /* variable for html-document */
		err  error              /* error variable */
	)

	/* load html-document (entry page) */
	tmpl, err = template.ParseFiles(filenameHTML)
	file.HandleErrorFile(err)

	/* send entry page to user (without error) */
	err = tmpl.Execute(writer, objectView)
	file.HandleErrorFile(err)

}

func CheckWebReference(request *http.Request, ipAddress string, port string, path string) bool {

	return request.Referer() != "http://" +ipAddress+ ":" + port + path

}