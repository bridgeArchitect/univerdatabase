package crypt

/* constant for hash */
const (
	A = 171
	B = 11213
	M = 53125
)

/* function to calculate hash of row */
func CalculateHash(row string) int {

	var (
		i    int /* iterator */
		hash int /* variable for hash */
	)

	/* calculate hash */
	i = 0
	hash = 0
	for i < len(row) {
		hash += (A*int(row[i]) + B) % M
		i++
	}
	hash %= M

	/* return hash */
	return hash

}
