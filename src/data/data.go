package data

import (
	"database/sql"
	"file"
)

/* constant for array */
const (
	sizeArray = 100
)

/*type "User" for database */
type User struct {
	Id    int
	Name  string
	Passw int
}

/*type "Student" for database */
type Student struct {
	Id        int
	Name      string
	Surname   string
	DateBirth string
	Email     string
}

/* structure to handle database */
type HandlerDatabase struct {
	db *sql.DB
}

/* constructor for type "HandlerDatabase" */
func NewHandlerDatabase(nameDB string, dataDB string) (*HandlerDatabase, error) {

	var (
		handlerDatabase *HandlerDatabase /* handler of database */
		db	            *sql.DB			 /* handler of database */
		err             error			 /* variable for error */
	)

	/* create object */
	handlerDatabase = new(HandlerDatabase)

	/* open database and check error */
	db, err = sql.Open(nameDB, dataDB)
	if err != nil {
		return nil, err
	}

	/* save connection and return object */
	handlerDatabase.db = db
	return handlerDatabase, nil

}

/* function to check user in database */
func (handler *HandlerDatabase) CheckUserDatabase(login string, hash int) bool {

	var (
		rows     *sql.Rows /* rows of database */
		err      error     /* variable for error */
	)

	/* make query to find user */
	rows, err = (*handler).db.Query("select * from users where login = ? && password = ?", login, hash)
	file.HandleErrorFile(err)

	/* check user and return answer */
	if rows.Next() {
		return true
	}
	return false

}

/* function to receive name of tables */
func (handler *HandlerDatabase) GetTables() []string {

	var (
		rows     *sql.Rows /* rows of database */
		err      error     /* variable for error */
		names    []string  /* names of tables */
		curName  string    /* current name */
	)

	/* make query to get tables */
	rows, err = (*handler).db.Query("show tables")
	file.HandleErrorFile(err)

	/* create array of names */
	names = make([]string, 0, sizeArray)

	/* receive names of table */
	for rows.Next() {
		err = rows.Scan(&curName)
		file.HandleErrorFile(err)
		names = append(names, curName)
	}

	/* return answer */
	return names

}

/* function to get users from database */
func (handler *HandlerDatabase) GetUsers() *[]User {

	var (
		rows     *sql.Rows /* rows of database */
		err      error     /* variable for error */
		users    []User    /* users of system */
		curUser  User      /* current user */
	)

	/* make query to get users */
	rows, err = (*handler).db.Query("select * from users")
	file.HandleErrorFile(err)

	/* create array of users */
	users = make([]User, 0, sizeArray)

	/* receive users */
	for rows.Next() {
		err = rows.Scan(&curUser.Id, &curUser.Name, &curUser.Passw)
		file.HandleErrorFile(err)
		users = append(users, curUser)
	}

	/* return users */
	return &users

}

/* function to get students from database */
func (handler *HandlerDatabase) GetStudents() *[]Student {

	var (
		rows         *sql.Rows /* rows of database */
		err          error     /* variable for error */
		students     []Student /* students of system */
		curStudent   Student   /* current student */
	)

	/* make query to get students */
	rows, err = (*handler).db.Query("select * from students")
	file.HandleErrorFile(err)

	/* create array of students */
	students = make([]Student, 0, sizeArray)

	/* receive students */
	for rows.Next() {
		err = rows.Scan(&curStudent.Id, &curStudent.Name, &curStudent.Surname, &curStudent.DateBirth, &curStudent.Email)
		file.HandleErrorFile(err)
		students = append(students, curStudent)
	}

	/* return students */
	return &students

}

/* function to close handler of database */
func (handler *HandlerDatabase) Close() {

	var (
		err error /* variable for error */
	)

	/* close database */
	err = (*handler).db.Close()
	file.HandleErrorFile(err)

}